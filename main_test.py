import unittest

import main


class TestRsa(unittest.TestCase):

    def test_rsa(self):
        message = "to be or not to be"
        bits = 256
        e, d, n = main.find_rsa_key(bits)
        encrypted = main.message_encrypt(message, e, n)
        print(f'Encrypted message: {encrypted}')
        decrypted = main.message_decrypt(encrypted, d, n)
        self.assertEqual(message, decrypted)


if __name__ == '__main__':
    unittest.main()
