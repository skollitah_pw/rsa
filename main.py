from random import randrange
from sys import byteorder


def e_gcd(a, b):
    x, y, u, v = 0, 1, 1, 0
    while a != 0:
        q, r = b // a, b % a
        m, n = x - u * q, y - v * q
        b, a, x, y, u, v = a, r, u, v, m, n
    gcd = b
    return gcd, x, y


def modular_inverse(a, z):
    gcd, x, y = e_gcd(a, z)
    return x % z


def miller_rabin(n, k):
    if n == 2 or n == 3:
        return True

    if n <= 1 or n % 2 == 0:
        return False

    r = 0
    s = n - 1

    while s % 2 == 0:
        r += 1
        s //= 2

    for _ in range(k):
        a = randrange(2, n - 1)
        x = pow(a, s, n)

        if x == 1 or x == n - 1:
            continue

        if is_composite(r, x, n):
            return False

    return True


def is_composite(r, x, n):
    for _ in range(r - 1):
        x = pow(x, 2, n)
        if x == n - 1:
            return False

    return True


def find_prime(bits, trials=1000):
    min = 2 ** (bits - 1)
    max = 2 ** bits

    for i in range(trials):
        candidate = randrange(min, max)

        if miller_rabin(candidate, 40):
            print(f'Prime number found after {i} iterations')
            return candidate

    raise Exception('Prime number not found')


def find_relative_prime(n, trials):
    for _ in range(trials):
        e = randrange(2, n - 1)
        gcd, x, y = e_gcd(e, n)

        if gcd == 1:
            return e

    raise Exception('Relative prime not found')


def find_rsa_key(bits):
    p = find_prime(bits)
    q = find_prime(bits)
    n = p * q
    euler = (p - 1) * (q - 1)

    e = find_relative_prime(euler, 40)
    d = modular_inverse(e, euler)

    if (e * d) % euler != 1:
        raise Exception('Invalid key computation')

    return e, d, n


def rsa_encrypt(m, e, n):
    return pow(m, e, n)


def rsa_decrypt(c, d, n):
    return pow(c, d, n)


def text_to_bytes(text):
    return bytes(text, encoding='utf-8')


def bytes_to_text(b):
    return b.decode('utf-8')


def num_to_bytes(num):
    return num.to_bytes((num.bit_length() + 7) // 8, byteorder, signed=False)


def bytes_to_num(b):
    return int.from_bytes(b, byteorder, signed=False)


def message_encrypt(message, e, n):
    b = text_to_bytes(message)
    number = bytes_to_num(b)
    encrypted_number = rsa_encrypt(number, e, n)
    b_encrypted = num_to_bytes(encrypted_number)
    return b_encrypted.hex()


def message_decrypt(cipher, d, n):
    b_encrypted = bytes.fromhex(cipher)
    encrypted_number = bytes_to_num(b_encrypted)
    number = rsa_decrypt(encrypted_number, d, n)
    b = num_to_bytes(number)
    return bytes_to_text(b)


if __name__ == '__main__':
    try:
        operation = input("operation e - encrypt, d - decrypt: ")

        if operation == "e":
            bits = int(input("Key bits: "))
            message = input("Message: ")
            e, d, n = find_rsa_key(bits)
            print(f'Public Key: e={e}, n={n}')
            print(f'Private Key: d={d}, n={n}')
            print(f"Encoded message (hex bytes): {message_encrypt(message, e, n)}")
        elif operation == "d":
            cipher = input('Cipher (hex bytes): ')
            d = int(input('Private Key d: '))
            n = int(input('Private Key n: '))
            print(f"Decoded message: {message_decrypt(cipher, d, n)}")
    except Exception as e:
        raise e
